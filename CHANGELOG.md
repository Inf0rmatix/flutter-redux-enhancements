## 1.0.5

Adding utility function reduceRoot that always returns non-null Parent state

## 1.0.4 

Finalized tests

## 1.0.3+2

More improvements to documentation

## 1.0.3+1

Formatting, for the extra 10 pub points :)

## 1.0.3

Further improvements to documentation

## 1.0.2

Adding missing library documentation

## 1.0.1 

Added a proper example

## 1.0.0

Initial release, working setup with tests.
Example is a placeholder right now
