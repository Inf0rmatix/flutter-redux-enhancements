/// Redux Enhancement aims to provide an opiniotated approach for complex redux applications
///
/// For every class that is used within the state, a [ReduxReducer] is created.
/// These classes can be infinitely nested, as long as every Action (classes that extend [ReduxAction]) has at maximum ONE reducer for every possible signature.
/// Having multiple reducers will not produce errors, but the first that is found wins which will result in unexpected behaviour.
///
/// ```
/// class Customer {
///   String name = '';
/// }
///
/// class AppState {
///   Customer customer = Customer();
/// }
///
/// class UpdateCustomerNameAction extends ReduxAction<String> {
///   UpdateCustomerNameAction(String value) : super(value);
/// }
///
/// class CustomerReducer extends ReduxReducer<AppState, Customer> {
///   CustomerReducer()
///       : super(
///     getter: (appStateBuilder) => appStateBuilder.customer,
///     setter: (appStateBuilder, customer) => appStateBuilder..customer = customer,
///     reducers: [
///       ActionReducer<Customer, UpdateCustomerNameAction, String>(_OnUpdateCustomerNameAction),
///     ],
///     children: [],
///   );
///
///   static Customer _OnUpdateCustomerNameAction(Customer customer, String name) => customer..name = name;
/// }
///
/// class AppStateReducer extends ReduxReducer<AppState, AppState> {
///   AppStateReducer()
///       : super(
///     getter: (state) => state,
///     setter: (state, value) => value,
///     reducers: [],
///     children: [
///       CustomerReducer(),
///     ],
///   );
/// }
///
/// ```
///
///
library redux_enhancement;

export 'src/action_reducer.dart';
export 'src/redux_action.dart';
export 'src/redux_reducer.dart';
export 'src/redux_types.dart';
