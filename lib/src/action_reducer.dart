import 'package:redux_enhancement/src/redux_types.dart';

/// Logical representation of an [Action] linked to a [SetterFunction]
class ActionReducer<State, Action, Value> {
  final Type action = Action;
  final SetterFunction<State, Value> reducer;

  ActionReducer(this.reducer);

  State call(State state, Value value) => reducer(state, value);
}
