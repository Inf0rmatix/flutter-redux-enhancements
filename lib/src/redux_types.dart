import 'package:redux_enhancement/src/redux_action.dart';

/// Named Function Signature that takes an object of [Type] and returns a value of [Value]
typedef GetterFunction<Type, Value> = Value Function(Type);

/// Named Function Signature that takes an object of [Type] and sets a property of type [Value]
typedef SetterFunction<Type, Value> = Type Function(Type, Value);

/// Named Function Signature that is used within [ReduxReducer.reducers]
typedef ReducerFunction<Type, Value> = Type Function(Type, ReduxAction<Value>);

/// [Root] - Type of the root object that will be changed
///
/// [Intermediate] - Type of the intermediate object that holds the property to change
///
/// [Value] - Type of the property that shall be changed
///
/// Combines two functions and returns one strong-typed function that
/// takes an object of type [Root] and a value of type [Value] and replaces
/// a property in [Intermediate] which is is received by calling [rootGetter] with an object of type [Root]
SetterFunction<Root, Value> combineSetterFunctions<Root, Intermediate, Value>(
  SetterFunction<Root, Intermediate> rootSetter,
  SetterFunction<Intermediate, Value> propertySetter,
  GetterFunction<Root, Intermediate> rootGetter,
) {
  SetterFunction<Root, Value> function = (Root builder, Value value) {
    Intermediate changedObject = propertySetter(rootGetter(builder), value);
    Root changedRoot = rootSetter(builder, changedObject);

    return changedRoot;
  };

  return function;
}
