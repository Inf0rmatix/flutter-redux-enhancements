import 'package:redux_enhancement/src/action_reducer.dart';
import 'package:redux_enhancement/src/redux_action.dart';
import 'package:redux_enhancement/src/redux_types.dart';

/// Every Reducer has to extend this class
/// ```
/// class ShopReducer extends ReduxReducer<AppState, Shop> {
///   static Map<Type, ReducerFunction<AppState, dynamic>> _reducers = {
///     UpdateShopNameAction: (shop, action) => shop..name = action.value,
///   }
///
///   ShopReducer()
///       : super(
///     getter: (appState) => appState.shop,
///     setter: (appState, value) => appState..shop = value,
///     reducers: _reducers,
///     children: [
///       AddressReducer<Shop>(
///             (shop) => shop.address,
///             (shop, address) => shop..address = address,
///       ),
///     ],
///   );
/// }
/// ```
///
abstract class ReduxReducer<Parent, ClassType> {
  GetterFunction<Parent, ClassType> getter;

  SetterFunction<Parent, ClassType> setter;

  List<ReduxReducer<ClassType, dynamic>> children;

  List<ActionReducer<ClassType, dynamic, dynamic>> reducers;

  ReduxReducer({
    required this.getter,
    required this.setter,
    required this.reducers,
    this.children = const [],
  });

  Parent reduceRoot(Parent parentState, ReduxAction action) {
    Parent? changedState = reduce(parentState, action);

    if (changedState == null) {
      print('No reducer found for ${action.runtimeType}');
    }

    return changedState ?? parentState;
  }

  /// Retrieves the fitting reducer for the [ReduxAction] supplied, executes it and returns the [Parent]
  /// or null if no [ReducerFunction] was found in its [reducers] or within its [children]
  /// The first [ReducerFunction] that is found is used and the result returned immediately.
  /// You should never have multiple [ReducerFunction]s defined for any [ReduxAction]!
  Parent? reduce(Parent parentState, ReduxAction action) {
    ClassType state = getter(parentState);

    Iterable<ActionReducer> result = reducers.where((element) => element.action == action.runtimeType);

    ActionReducer? actionReducer = result.isNotEmpty ? result.first : null;

    if (actionReducer != null) {
      return setter(parentState, actionReducer.call(state, action.value));
    }

    for (ReduxReducer<ClassType, dynamic> childReducer in children) {
      ClassType? newChildState = childReducer.reduce(state, action);

      if (newChildState != null) {
        return setter(parentState, newChildState);
      }
    }
  }
}
