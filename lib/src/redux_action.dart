import 'package:redux_enhancement/src/redux_reducer.dart';

/// Abstract class every Action that is used with this packages [ReduxReducer] must extend.
/// Use the IDE to your advantage and let the constructor be auto-generated - less typing!
/// ```
/// import 'package:redux_enhancement/redux_action.dart';
///
/// class UpdateShopNameAction extends ReduxAction<String> {
///   UpdateShopNameAction(String value) : super(value);
/// }
/// ```
///
/// You can even make actions more generic, which comes in handy when multiple classes have propertys of the same type,
/// like an address for example
/// ```
/// import 'package:redux_enhancement/redux_action.dart';
///
/// class UpdateFirstNameAction<Parent> extends ReduxAction<String> {
///   UpdateFirstNameAction(String value) : super(value);
/// }
/// ```
///
/// The type parameter will then be passed down from the AddressReducer constructor into the Reducer Functions
/// For more details how that's done, take a look at the example code (see the class "AddressReducer")
abstract class ReduxAction<T> {
  final Type type;
  final T value;

  ReduxAction(this.value) : type = T;
}
