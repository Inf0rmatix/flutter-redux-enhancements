import 'package:redux_enhancement/redux_enhancement.dart';

import '../../model/customer.dart';
import '../address/address_reducer.dart';
import '../app/app_state.dart';
import 'customer_actions.dart';

class CustomerReducer extends ReduxReducer<AppState, Customer> {
  CustomerReducer()
      : super(
          getter: (appStateBuilder) => appStateBuilder.customer,
          setter: (appStateBuilder, customer) => appStateBuilder..customer = customer,
          reducers: [
            ActionReducer<Customer, UpdateCustomerIdAction, int>(_OnUpdateCustomerIdAction),
          ],
          children: [
            AddressReducer<Customer>(
              (customerBuilder) => customerBuilder.address,
              (customerBuilder, address) => customerBuilder..address = address,
            ),
          ],
        );

  static Customer _OnUpdateCustomerIdAction(Customer customer, int id) => customer..id = id;
}
