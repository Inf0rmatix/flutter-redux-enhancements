import 'package:redux_enhancement/redux_enhancement.dart';

class UpdateCustomerIdAction extends ReduxAction<int> {
  UpdateCustomerIdAction(int value) : super(value);
}
