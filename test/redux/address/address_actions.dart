import 'package:redux_enhancement/redux_enhancement.dart';

class ChangeFirstNameAction<Parent> extends ReduxAction<String> {
  ChangeFirstNameAction(String value) : super(value);
}

class ChangeLastNameAction<Parent> extends ReduxAction<String> {
  ChangeLastNameAction(String value) : super(value);
}

class ChangeStreetAction<Parent> extends ReduxAction<String> {
  ChangeStreetAction(String value) : super(value);
}
