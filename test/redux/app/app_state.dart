library app_state;

import '../../model/customer.dart';
import '../../model/shop.dart';

class AppState {
  bool initialized = false;

  Shop shop = Shop();

  Customer customer = Customer();
}
