import 'package:redux_enhancement/redux_enhancement.dart';

class InitializeAction extends ReduxAction<void> {
  InitializeAction() : super(null);
}

class ActionWithoutAssignedReducer<Extender> extends ReduxAction<void> {
  ActionWithoutAssignedReducer() : super(null);
}
