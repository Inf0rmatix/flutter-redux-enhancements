import 'package:redux_enhancement/redux_enhancement.dart';

class UpdateShopNameAction extends ReduxAction<String> {
  UpdateShopNameAction(String value) : super(value);
}
