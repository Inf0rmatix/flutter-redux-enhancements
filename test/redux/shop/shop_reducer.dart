import 'package:redux_enhancement/redux_enhancement.dart';

import '../../model/shop.dart';
import '../address/address_reducer.dart';
import '../app/app_state.dart';
import 'shop_actions.dart';

class ShopReducer extends ReduxReducer<AppState, Shop> {
  ShopReducer()
      : super(
          getter: (appState) => appState.shop,
          setter: (appState, value) => appState..shop = value,
          reducers: [
            ActionReducer<Shop, UpdateShopNameAction, String>(_OnUpdateShopNameAction),
          ],
          children: [
            AddressReducer<Shop>(
              (shop) => shop.address,
              (shop, address) => shop..address = address,
            ),
          ],
        );

  static Shop _OnUpdateShopNameAction(Shop shop, String name) => shop..name = name;
}
