import 'package:redux/redux.dart';
import 'package:redux_enhancement/redux_enhancement.dart';
import 'package:test/test.dart';

import 'model/address.dart';
import 'model/customer.dart';
import 'model/shop.dart';
import 'redux/address/address_actions.dart';
import 'redux/app/app_state.dart';
import 'redux/app/app_state_actions.dart';
import 'redux/app/app_state_reducer.dart';
import 'redux/customer/customer_actions.dart';
import 'redux/shop/shop_actions.dart';

void main() {
  late Store<AppState> store;

  group('Redux Enhancement', () {
    setUp(() {
      store = Store<AppState>(
        (state, action) => AppStateReducer().reduceRoot(state, action),
        initialState: AppState(),
      );
    });

    tearDown(() async {
      await store.teardown();
    });

    /// This group is mostly to prove the type-safety
    group('Redux Types', () {
      test('Getter Function should get', () {
        GetterFunction<Shop, String> nameGetter = (shop) => shop.name;

        Shop shop = Shop();
        shop.name = 'Test';

        expect(nameGetter(shop), equals(shop.name));
      });

      test('Setter Function should set', () {
        SetterFunction<Shop, String> nameSetter = (shop, name) => shop..name = name;

        Shop shop = Shop();

        expect(shop.name, equals(''));
        expect(nameSetter(shop, 'Test').name, equals('Test'));
      });

      test('Combine Setter Functions should work', () {
        SetterFunction<AppState, Shop> shopSetter = (appState, shop) => appState..shop = shop;
        SetterFunction<Shop, Address> shopAddressSetter = (shop, address) => shop..address = address;
        GetterFunction<AppState, Shop> shopGetter = (appState) => appState.shop;

        SetterFunction<AppState, Address> combinedSetter = combineSetterFunctions<AppState, Shop, Address>(
          shopSetter,
          shopAddressSetter,
          shopGetter,
        );

        AppState appState = AppState();
        appState.shop.address.firstName = 'test';

        appState = combinedSetter(appState, Address());

        expect(appState.shop.address.firstName, isEmpty);
      });
    });

    group('Redux Reducer', () {
      test('should reduce', () {
        store.dispatch(InitializeAction());

        expect(store.state.initialized, isTrue);
      });

      test('should not fail if no reducer found', () {
        AppState previousState = store.state;

        store.dispatch(ActionWithoutAssignedReducer<String>());

        expect(store.state, equals(previousState));
      });

      test('should use nested reducers', () {
        store.dispatch(UpdateCustomerIdAction(42));

        expect(store.state.customer.id, equals(42));

        store.dispatch(UpdateShopNameAction('Megashop'));

        expect(store.state.shop.name, equals('Megashop'));
      });

      test('should be typesafe', () {
        store.dispatch(ChangeFirstNameAction<Shop>('Test'));
        store.dispatch(ChangeFirstNameAction<Customer>('Another Test'));

        expect(store.state.shop.address.firstName, equals('Test'));
        expect(store.state.customer.address.firstName, equals('Another Test'));
      });
    });
  });
}
