# redux_enhancement

An objective approach for modular and easy-to-use redux convenience classes

The aim is to enable most-modular reducer design while avoiding having to declare global functions
as suggested in the docs of the redux package.

Following is a short example. The address reducer could be reused by other reducers. All nested and modularized.
The whole code of this example can be found in the projects "test" directory.
An interactive test application written with Flutter can be found in the "example" directory.

```dart
class Main {
  void main() {
    Store<AppState> store = Store<AppState>(
          (state, action) => AppStateReducer().reduceRoot(state, action),
      initialState: AppState(),
    );
  }
}

class AppState {
  bool initialized = false;

  Shop shop = Shop();

  Customer customer = Customer();
}

class AppStateReducer extends ReduxReducer<AppState, AppState> {
  AppStateReducer()
      : super(
    getter: (state) => state,
    setter: (state, value) => value,
    reducers: [],
    children: [
      CustomerReducer(),
    ],
  );
}

class UpdateCustomerIdAction extends ReduxAction<int> {
  UpdateCustomerIdAction(int value) : super(value);
}

class CustomerReducer extends ReduxReducer<AppState, Customer> {
  CustomerReducer()
      : super(
    getter: (appStateBuilder) => appStateBuilder.customer,
    setter: (appStateBuilder, customer) => appStateBuilder..customer = customer,
    reducers: [
      ActionReducer<Customer, UpdateCustomerIdAction, int>(_OnUpdateCustomerIdAction),
    ],
    children: [
      AddressReducer<Customer>(
            (customerBuilder) => customerBuilder.address,
            (customerBuilder, address) => customerBuilder..address = address,
      ),
    ],
  );

  static Customer _OnUpdateCustomerIdAction(Customer customer, int id) => customer..id = id;
}

class ChangeFirstNameAction<Parent> extends ReduxAction<String> {
  ChangeFirstNameAction(String value) : super(value);
}

class ChangeLastNameAction<Parent> extends ReduxAction<String> {
  ChangeLastNameAction(String value) : super(value);
}

class ChangeStreetAction<Parent> extends ReduxAction<String> {
  ChangeStreetAction(String value) : super(value);
}

class AddressReducer<Parent> extends ReduxReducer<Parent, Address> {
  AddressReducer(GetterFunction<Parent, Address> getter,
      SetterFunction<Parent, Address> setter,) : super(
    getter: getter,
    setter: setter,
    reducers: [
      ActionReducer<Address, ChangeFirstNameAction<Parent>, String>(_OnChangeFirstNameAction),
      ActionReducer<Address, ChangeLastNameAction<Parent>, String>(_OnChangeLastNameAction),
      ActionReducer<Address, ChangeStreetAction<Parent>, String>(_OnChangeStreetAction),
    ],
  );

  static Address _OnChangeFirstNameAction(Address address, String firstName) =>
      address..firstName = firstName;

  static Address _OnChangeLastNameAction(Address address, String lastName) =>
      address..lastName = lastName;

  static Address _OnChangeStreetAction(Address address, String street) => address..street = street;
}
```
