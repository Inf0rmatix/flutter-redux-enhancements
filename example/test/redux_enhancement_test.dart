import 'package:flutter_test/flutter_test.dart';
import 'package:redux/redux.dart';
import 'package:redux_enhancement/redux_enhancement.dart';

import '../lib/model/customer.dart';
import '../lib/model/shop.dart';
import '../lib/redux/address/address_actions.dart';
import '../lib/redux/app/app_state.dart';
import '../lib/redux/app/app_state_actions.dart';
import '../lib/redux/app/app_state_reducer.dart';
import '../lib/redux/customer/customer_actions.dart';
import '../lib/redux/shop/shop_actions.dart';

void main() {
  late Store<AppState> store;

  group('Redux Enhancement', () {
    setUp(() {
      store = Store<AppState>(
        (state, action) => AppStateReducer().reduce(state, action) ?? state,
        initialState: AppState(),
      );
    });

    tearDown(() async {
      await store.teardown();
    });

    /// This group is mostly to prove the type-safety
    group('Redux Types', () {
      test('Getter Function should get', () {
        GetterFunction<Shop, String> nameGetter = (shop) => shop.name;

        Shop shop = Shop();
        shop.name = 'Test';

        expect(nameGetter(shop), equals(shop.name));
      });

      test('Setter Function should set', () {
        SetterFunction<Shop, String> nameSetter = (shop, name) => shop..name = name;

        Shop shop = Shop();

        expect(shop.name, equals(''));
        expect(nameSetter(shop, 'Test').name, equals('Test'));
      });
    });

    group('Redux Reducer', () {
      test('should reduce', () {
        store.dispatch(InitializeAction());

        expect(store.state.initialized, isTrue);
      });

      test('should use nested reducers', () {
        store.dispatch(UpdateCustomerIdAction(42));

        expect(store.state.customer.id, equals(42));

        store.dispatch(UpdateShopNameAction('Megashop'));

        expect(store.state.shop.name, equals('Megashop'));
      });

      test('should be typesafe', () {
        store.dispatch(ChangeFirstNameAction<Shop>('Test'));
        store.dispatch(ChangeFirstNameAction<Customer>('Another Test'));

        expect(store.state.shop.address.firstName, equals('Test'));
        expect(store.state.customer.address.firstName, equals('Another Test'));
      });
    });
  });
}
