import 'package:example/model/shop.dart';
import 'package:example/redux/address/address_actions.dart';
import 'package:example/redux/app/app_state.dart';
import 'package:example/redux/app/app_state_reducer.dart';
import 'package:example/redux/shop/shop_actions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() {
  Store<AppState> store = Store<AppState>(
    (state, action) => AppStateReducer().reduce(state, action) ?? state,
    initialState: AppState(),
  );

  runApp(
    MyApp(
      store: store,
    ),
  );
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp({required this.store});

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        title: 'Redux Enhancements',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _name = 'Redux-Shop';
  String _shopStreet = 'Reductionstreet 42';

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Store<AppState> store = StoreProvider.of<AppState>(context);
      store.dispatch(UpdateShopNameAction(_name));
      store.dispatch(ChangeStreetAction<Shop>(_shopStreet));
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 400),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              StoreConnector<AppState, Shop>(
                converter: (store) => store.state.shop,
                builder: (BuildContext context, Shop shop) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Row(
                      children: [
                        Text(
                          '${shop.name}\n${shop.address.street}',
                          textScaleFactor: 1.5,
                        ),
                      ],
                    ),
                  );
                },
              ),
              TextFormField(
                initialValue: _name,
                onChanged: (value) => setState(() => _name = value),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Enter a new shop name',
                ),
              ),
              Container(height: 16),
              ElevatedButton.icon(
                onPressed: () {
                  StoreProvider.of<AppState>(context)
                      .dispatch(UpdateShopNameAction(_name));
                },
                icon: Icon(Icons.refresh),
                label: Text('Change shop name'),
              ),
              Container(height: 16),
              TextFormField(
                initialValue: _shopStreet,
                onChanged: (value) => setState(() => _shopStreet = value),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Enter a new shop street',
                ),
              ),
              Container(height: 16),
              ElevatedButton.icon(
                onPressed: () {
                  StoreProvider.of<AppState>(context)
                      .dispatch(ChangeStreetAction<Shop>(_shopStreet));
                },
                icon: Icon(Icons.refresh),
                label: Text('Change shop street'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
