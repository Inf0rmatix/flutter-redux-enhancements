import 'package:redux_enhancement/redux_enhancement.dart';

import '../customer/customer_reducer.dart';
import '../shop/shop_reducer.dart';
import 'app_state.dart';
import 'app_state_actions.dart';

class AppStateReducer extends ReduxReducer<AppState, AppState> {
  AppStateReducer()
      : super(
          getter: (state) => state,
          setter: (state, value) => value,
          reducers: [
            ActionReducer<AppState, InitializeAction, void>(_OnInitializeAction),
          ],
          children: [
            ShopReducer(),
            CustomerReducer(),
          ],
        );

  static AppState _OnInitializeAction(AppState appState, _) => appState..initialized = true;
}
