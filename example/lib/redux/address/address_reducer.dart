import 'package:redux_enhancement/redux_enhancement.dart';

import '../../model/address.dart';
import 'address_actions.dart';

class AddressReducer<Parent> extends ReduxReducer<Parent, Address> {
  AddressReducer(
    GetterFunction<Parent, Address> getter,
    SetterFunction<Parent, Address> setter,
  ) : super(
          getter: getter,
          setter: setter,
          reducers: [
            ActionReducer<Address, ChangeFirstNameAction<Parent>, String>(
                _OnChangeFirstNameAction),
            ActionReducer<Address, ChangeLastNameAction<Parent>, String>(
                _OnChangeLastNameAction),
            ActionReducer<Address, ChangeStreetAction<Parent>, String>(
                _OnChangeStreetAction),
          ],
        );

  static Address _OnChangeFirstNameAction(Address address, String firstName) =>
      address..firstName = firstName;
  static Address _OnChangeLastNameAction(Address address, String lastName) =>
      address..lastName = lastName;
  static Address _OnChangeStreetAction(Address address, String street) =>
      address..street = street;
}
